package ar.uba.fi.tdd.exercise;

public class AgedBrie extends Item {

	public AgedBrie(String _name, int _sellIn, int _quality) {
		super(_name, _sellIn, _quality);
	}
	
	int ONE_DAY_INCREASE = 1;

	@Override
	public void update() {
		Item.increaseQuality(this, ONE_DAY_INCREASE);
	}

}
