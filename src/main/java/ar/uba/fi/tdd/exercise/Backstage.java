package ar.uba.fi.tdd.exercise;

public class Backstage extends Item{

	public Backstage(String _name, int _sellIn, int _quality) {
		super(_name, _sellIn, _quality);
	}
	int INITIAL_INCREASE_FACTOR = 1;
	int CLOSER_INCREASE_FACTOR_SUM = 1;
	int FIRST_CLOSER_CONCERT_DEADLINE = 11;
	int LAST_CLOSER_CONCERT_DEADLINE = 6;
	
	@Override
	public void update() {
		if (this.sellIn<=0) {
			Item.degradeWholeQuality(this);
			return;
		}
		int factor= INITIAL_INCREASE_FACTOR;
		if(this.sellIn<FIRST_CLOSER_CONCERT_DEADLINE) factor += CLOSER_INCREASE_FACTOR_SUM;
		if(this.sellIn<LAST_CLOSER_CONCERT_DEADLINE) factor += CLOSER_INCREASE_FACTOR_SUM;
		Item.increaseQuality(this, factor);
	}

}
