package ar.uba.fi.tdd.exercise;

public class Sulfura extends Item {
	static final int SPECIAL_MAX_QUALITY_PERMITED = 80;
	public Sulfura(String _name, int _sellIn) {
		super(_name, _sellIn, SPECIAL_MAX_QUALITY_PERMITED);
	}

}
