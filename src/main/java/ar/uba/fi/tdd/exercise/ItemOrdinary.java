package ar.uba.fi.tdd.exercise;

public class ItemOrdinary extends Item {

	public ItemOrdinary(String _name, int _sellIn, int _quality) {
		super(_name, _sellIn, _quality);
	}

	@Override
	public void update() {
		Item.degradeQuality(this);
	}

}
