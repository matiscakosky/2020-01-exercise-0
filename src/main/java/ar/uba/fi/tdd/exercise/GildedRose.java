
package ar.uba.fi.tdd.exercise;

class GildedRose {
	Item[] items;
	

	
	public GildedRose(Item[] _items) {
		items = _items;
	}

	public void updateQuality() {
		for (Item item : items) {
			item.update();
			item.lowerSalesDay();
		}

	}


}
