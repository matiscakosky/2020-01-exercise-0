package ar.uba.fi.tdd.exercise;

public class Item {

    //name
    public String Name;

    //seell in
    public int sellIn;

    //quality
    public int quality;

    // constructor
    public Item(String _name, int _sellIn, int _quality) {
        this.Name = _name;
        this.sellIn = _sellIn;
        this.quality = _quality;
    }

    // shows the Item representation
   @Override
   public String toString() {

     return this.Name + ", " + this.sellIn + ", " + this.quality;
    }
   
	static final int MAX_QUALITY_PERMITED = 50;
	int DECREASE_SELL_FACTOR = 1;
	static int LASTEST_DAY_SELLIN = 0;
	static int NORMAL_QUALITIY = 1;
	static int FASTER_DECREASE_QUALITY = 2;
	static int NO_QUALITY = 0;
	static int NORMAL_DECREASE_QUALITIY = 1;
	
   
   public void update() {}
   
   public void degrade(int factor) {
	   this.quality -= factor;
   }
   
   public void stablish(int factor) {
	   this.quality = factor;
   }
   
   public void increase(int factor){
	   this.quality += factor;
   }
   
   public static void degradeQuality(Item item) {
		if (item.quality == NO_QUALITY)
			return;
		if ((item.sellIn) <= LASTEST_DAY_SELLIN && item.quality > NORMAL_QUALITIY) {
			item.degrade(FASTER_DECREASE_QUALITY);
		} else {
			item.degrade(NORMAL_DECREASE_QUALITIY);
		}
	}
   

	public static void degradeWholeQuality(Item i) {
		i.quality = 0;
	}

	public static void increaseQuality(Item item, int gain) {
		if ((item.quality + gain) > MAX_QUALITY_PERMITED) {
			item.stablish(MAX_QUALITY_PERMITED);
			return;
		}
		item.increase(gain);

	}

	public void lowerSalesDay() {
		this.sellIn -= DECREASE_SELL_FACTOR;
	}

   
   
 }
