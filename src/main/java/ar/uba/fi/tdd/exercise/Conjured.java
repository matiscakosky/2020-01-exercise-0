package ar.uba.fi.tdd.exercise;

public class Conjured extends Item {

	public Conjured(String _name, int _sellIn, int _quality) {
		super(_name, _sellIn, _quality);
	}
	
	static int LASTEST_DAY_SELLIN = 0;
	static int NORMAL_QUALITIY = 1;
	static int FASTER_DECREASE_QUALITY = 4;
	static int NO_QUALITY = 0;
	static int NORMAL_DECREASE_QUALITIY = 2;
	
	@Override
	public void update() {
		if (this.quality == NO_QUALITY)
			return;
		if ((this.sellIn) <= LASTEST_DAY_SELLIN && this.quality > NORMAL_QUALITIY) {
			this.quality -= FASTER_DECREASE_QUALITY;
		} else {
			this.quality -= NORMAL_DECREASE_QUALITIY;
		}

	}

}
