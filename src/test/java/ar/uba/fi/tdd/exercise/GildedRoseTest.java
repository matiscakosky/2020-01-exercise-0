package ar.uba.fi.tdd.exercise;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;

import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(Alphanumeric.class)
@SpringBootTest
class GildedRoseTest {

	@Test
	public void foo() {
			Item[] items = new Item[] { new ItemOrdinary("fixme", 0, 0) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat("fixme").isEqualTo(app.items[0].Name);
	}
	
	@Test
	public void test01comprueboSellInItemAntesDeUpdateQuality() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", 0, 0) };
		GildedRose app = new GildedRose(items);
		assertEquals(0,app.items[0].sellIn);
	}
	
	@Test
	public void test02comprueboQualityItem() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", 0, 1) };
		GildedRose app = new GildedRose(items);
		assertEquals(1, app.items[0].quality);
	}
	
	@Test
	public void test03comprueboQualityItemLuegoQuePasoFechaCaducidad() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", 0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, app.items[0].quality);
	}
	
	@Test
	public void test04comprueboQualityItemNoBajaDeCeroInclusoMuchoTiempoDespuesDeCaducidad() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", -3, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, app.items[0].quality);
	}
	
	@Test
	public void test05comprueboQualityDegradaDeAUnaUnidadObjetoComun() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", 10, 1000) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(999, app.items[0].quality);
	}
	
	@Test
	public void test06comprueboQualityDegradaDeADosUnidadesObjetoComunPasadaFechaCaducidad() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", 0, 1000) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(998, app.items[0].quality);
	}
	
	@Test
	public void test07comprueboQualityAmuentaLuegoDeUnDia() {
		Item[] items = new Item[] { new AgedBrie("Aged Brie", 10, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(11, app.items[0].quality);
	}
	
	
	@Test
	public void test08comprueboQualityNoAmuentaCalidadMaxima() {
		Item[] items = new Item[] { new AgedBrie("Aged Brie", 10, 50) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, app.items[0].quality);
	}
	
	@Test
	public void test09comprueboQualityNoCambiaCalidadSulfuras() {
		Item[] items = new Item[] { new Sulfura("Sulfuras, Hand of Ragnaros", 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(80, app.items[0].quality);
	}
	
	@Test
	public void test10comprueboQualityNoCambiaCalidadMaximaSulfuras() {
		Item[] items = new Item[] { new Sulfura("Sulfuras, Hand of Ragnaros", 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(80, app.items[0].quality);
	}
	
	@Test
	public void test11comprueboQualityNoCambiaCalidadMinimaSulfuras() {
		Item[] items = new Item[] { new Sulfura("Sulfuras, Hand of Ragnaros", 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(80, app.items[0].quality);
	}
	
	@Test
	public void test12comprueboQualityAumenta2ConcertFaltando10Dias() {
		Item[] items = new Item[] { new Backstage("Backstage to a TAFKAL80ETC concert", 10, 25) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(27, app.items[0].quality);
	}
	
	@Test
	public void test13comprueboQualityAumenta1ConcertFaltando10Dias() {
		Item[] items = new Item[] { new Backstage("Backstage to a TAFKAL80ETC concert", 10, 49) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, app.items[0].quality);
	}
	
	@Test
	public void test14comprueboQualityAumenta3ConcertFaltando5Dias() {
		Item[] items = new Item[] { new Backstage("Backstage to a TAFKAL80ETC concert", 5, 25) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(28, app.items[0].quality);
	}
	
	@Test
	public void test15comprueboQualityAumenta2ConcertFaltando5Dias() {
		Item[] items = new Item[] { new Backstage("Backstage to a TAFKAL80ETC concert", 5, 48) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, app.items[0].quality);
	}
	
	@Test
	public void test16comprueboQualityAumenta1ConcertFaltando5Dias() {
		Item[] items = new Item[] { new Backstage("Backstage to a TAFKAL80ETC concert", 5, 49) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, app.items[0].quality);
	}
	
	@Test
	public void test17comprueboQualityVaA0ConcertFaltandoPasadoConcierto() {
		Item[] items = new Item[] { new Backstage("Backstage to a TAFKAL80ETC concert", 0, 49) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, app.items[0].quality);
	}
	
	@Test
	public void test18comprueboQualityDegradaDeAUnaUnidadVariosObjetos() {
		Item[] items = new Item[] { new ItemOrdinary("fixme", 10, 1000),new ItemOrdinary("fixme2",10,999) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(999, app.items[0].quality);
		assertEquals(998, app.items[1].quality);
	}
	
	@Test
	public void test19comprueboQualityDegradaDeAUnaPasadaFechaCaducidad() {
		Item[] items = new Item[] { new ItemOrdinary("fixme2",0,1) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, app.items[0].quality);
	}
	
	@Test
	public void test20comprueboQualityReducidaLuegoDeDosDias() {
		Item[] items = new Item[] { new ItemOrdinary("fixme2",5,10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertEquals(8, app.items[0].quality);
	}
	@Test
	public void test20comprueboQualityReducidaLuegoDeVariosDiasNoBajaDe0() {
		Item[] items = new Item[] { new ItemOrdinary("fixme2",3,3) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertEquals(0, app.items[0].quality);
	}
	
	@Test
	public void test21comprueboQualityReducidaDobleConjured() {
		Item[] items = new Item[] { new Conjured("fixme2",5,10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertEquals(6, app.items[0].quality);
	}
	
	@Test
	public void test22comprueboQualityReducidaDobleConjuredPasadaFechaCaducidad() {
		Item[] items = new Item[] { new Conjured("fixme2",0,20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertEquals(12, app.items[0].quality);
	}
	


}
